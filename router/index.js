const express = require("express");
const router = express.Router();
const bodyparser = require("body-parser");

router.get("/formulario",(req,res)=>{
    
    
    const valores = {
        numBoleto : req.query.numBoleto,
        destino : req.query.destino,
        nomCliente : req.query.nomCliente ,
        tipo : req.query.tipo,
        precio : req.query.precio,
        años : req.query.años 
    }

    res.render('formulario.html',valores)

})

router.post("/formulario",(req,res)=>{

    const valores = {
        numBoleto : req.body.numBoleto,
        destino : req.body.destino,
        nomCliente : req.body.nomCliente,
        tipo : req.body.tipo,
        precio : req.body.precio,
        años : req.body.años
    }

    res.render('formulario.html',valores)

})

module.exports=router;


